from django.db import models

class Product(models.Model):
    nome = models.CharField(max_length=45)
    descricao = models.TextField()
    imagem = models.ImageField(upload_to='produtos')
    quantidade = models.PositiveIntegerField()
    codigo = models.CharField(max_length=20)

    def __str__(self):
        return self.codigo + ' - ' + self.nome